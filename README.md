## promize

promize app on blockchain

## install

we are using pods to setup the libraries

```
# clone
git clone git@github.com:senzlab/promize-ios.git promize
cd promize

# since we are using pods
open Promize.xcworkspace
```

## setup pods

manually setup pods via

```
pod install --repo-update
```
