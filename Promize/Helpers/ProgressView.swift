//
//  ProgressView.swift
//  Promize
//
//  Created by Isuru on 1/30/19.
//  Copyright © 2019 Creative Solutions. All rights reserved.
//

import MBProgressHUD

// WARNING: - In the initial all launch, the background overlay
// view doesn't cover the navigation bar.
open class ProgressView {
    
    //    fileprivate var progressView: MBProgressHUD!
    
    open class var shared: ProgressView {
        struct Static {
            static let instance: ProgressView = ProgressView()
        }
        return Static.instance
    }
    
    //    static let shared = ProgressView()
    
    /**
     Show a progress view in the given view.
     Can show a title and/or subtitle as well.
     
     - parameter view:       View to be displayed in.
     - parameter mainText:   Title.
     - parameter detailText: Subtitle.
     */
    open func show(_ view: UIView, mainText: String?, detailText: String?) {
        
        if let hudView = MBProgressHUD.showAdded(to: view, animated: true){
            hudView.labelText = mainText ?? ""
            hudView.labelColor = UIColor.fromHex(HexColors.PRIMARY_COLOR.rawValue)
            hudView.detailsLabelText = detailText ?? ""
            hudView.detailsLabelColor = UIColor.black
            hudView.dimBackground = true
            hudView.isSquare = true
            hudView.color = UIColor.white
            hudView.activityIndicatorColor = UIColor.lightGray
        }
        
    }
    
    /**
     Hide the progress view.
     */
    open func hide(view: UIView) {
        MBProgressHUD.hideAllHUDs(for: view, animated: true)
        
    }
}

