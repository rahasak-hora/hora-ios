//
//  UIViewExtention.swift
//  Promize
//
//  Created by AnujAroshA on 5/16/18.
//  Copyright © 2018 Creative Solutions. All rights reserved.
//

import Foundation
import UIKit

extension UIView {
    
//    @IBInspectable var borderWidth: Float {
//        get {
//            return self.borderWidth
//        }
//        set {
//            self.layer.borderWidth = CGFloat(newValue)
//        }
//    }
//    
//    @IBInspectable var borderColor: UIColor {
//        get {
//            return self.borderColor
//        }
//        set {
//            self.layer.borderColor = newValue.cgColor
//        }
//    }

    
    func takeSnapshot() -> UIImage {
        UIGraphicsBeginImageContextWithOptions(bounds.size, false, UIScreen.main.scale)
        
        drawHierarchy(in: self.bounds, afterScreenUpdates: true)
        
        let image = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        return image
    }
}

extension CALayer {
    var borderColorFromUIColor: UIColor {
        get {
            return UIColor(cgColor: self.borderColor!)
        }
        
        set (color) {
            self.borderColor = color.cgColor
        }
    }
    
}
