//
//  TransferWatchViewController.swift
//  Promize
//
//  Created by Isuru on 3/10/19.
//  Copyright © 2019 Creative Solutions. All rights reserved.
//

import UIKit

class TransferWatchViewController: BaseViewController {

    @IBOutlet weak var doneButton: UIButton!
    @IBOutlet weak var transferButton: UIButton!
    
    @IBOutlet weak var modelName: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var referanceNo: UILabel!
    
    @IBOutlet weak var watchImageView: UIImageView!
    
    @IBOutlet weak var usernameTextField: UITextField!
    @IBOutlet weak var noteTextView: UITextView!
    var selectedWatch: Watch!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setupView()
        
        modelName.text = selectedWatch.model
        descriptionLabel.text = selectedWatch.watchDescription
        referanceNo.text = selectedWatch.referenceNo
        watchImageView.image = UIImage(named: selectedWatch.imageURL)
    }
    
    private func setupView(){
        
        // setup Done, Save button
        doneButton.backgroundColor = UIColor.clear
        doneButton.layer.cornerRadius = 8.0
        doneButton.layer.masksToBounds = true
        doneButton.layer.borderWidth = 1
        doneButton.layer.borderColor = UIColor.white.cgColor
        
        transferButton.backgroundColor = UIColor.clear
        transferButton.layer.cornerRadius = 8.0
        transferButton.layer.masksToBounds = true
        transferButton.layer.borderWidth = 1
        transferButton.layer.borderColor = UIColor.white.cgColor
        
        noteTextView.layer.cornerRadius = 8
        noteTextView.layer.borderColor = UIColor(red: 218/255, green: 218/255, blue: 218/255, alpha: 1.0).cgColor
        noteTextView.layer.borderWidth = 1
    }

    
    @IBAction func tappedtransferButton(_ sender: Any) {
        
        self.dismiss(animated: true) {
            
        }
    }
    
    @IBAction func tappedDoneButton(_ sender: Any) {
        
        self.dismiss(animated: true) {
            
        }
    }
    
}
