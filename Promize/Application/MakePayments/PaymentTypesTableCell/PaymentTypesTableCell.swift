//
//  TransferFundsTableViewCell.swift
//  Promize
//
//  Created by Arosha Piyadigama on 11/16/18.
//  Copyright © 2018 AnujAroshA. All rights reserved.
//

import UIKit

class PaymentTypesTableCell: UITableViewCell {
    
    @IBOutlet weak var nameLabel: UILabel!

    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
