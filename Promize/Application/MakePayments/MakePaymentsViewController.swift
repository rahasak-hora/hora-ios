//
//  TransferFundsViewController.swift
//  Promize
//
//  Created by Arosha Piyadigama on 11/16/18.
//  Copyright © 2018 Creative Solutions. All rights reserved.
//

import UIKit

class MakePaymentsViewController:BaseViewController, UITableViewDelegate, UITableViewDataSource, UISearchResultsUpdating, UISearchBarDelegate {
    
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var searchPlaceHolder: UIView!
    @IBOutlet weak var plusButton: UIButton!
    
    var searchBar: CustomSearchBar!
    let HEIGHT_OF_ROW = 70
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setupUi()
    }
    
    // MARK: UITableViewDataSource
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        // Try to find reusable cell
        var cell = tableView.dequeueReusableCell(withIdentifier: "MakePaymentsTableCell") as? MakePaymentsTableCell

        // If not available instantiate new custom cell
        if (cell == nil) {
            var nibArray = NSArray()
            nibArray = Bundle.main.loadNibNamed("MakePaymentsTableCell", owner: self, options: nil)! as NSArray
            cell = nibArray.object(at: 0) as? MakePaymentsTableCell
        }

        // Setup Cells in table
        cell?.nameLabel?.text = "Electricity"
        cell?.accountNumberLabel?.text = "Teloc electicle"
        cell?.bankNameLabel.isHidden = true

        cell?.selectionStyle = .none
        return cell!
    }

    // MARK: UITableViewDelegate
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return CGFloat(HEIGHT_OF_ROW)
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

    }

    // MARK: UISearchResultsUpdating
    func updateSearchResults(for searchController: UISearchController) {

    }
    
    // MARK: UISearchBarDelegate
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {

    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {

        self.searchBar.text = ""
        searchBar.resignFirstResponder()
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {

    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {

    }
    
    @IBAction func plusButtonAction(_ sender: UIButton) {
        let viewController = PaymentTypesViewController(nibName: "PaymentTypesViewController", bundle: nil)
        self.navigationController?.pushViewController(viewController, animated: false)
    }
    
    //    MARK: Supportive methods
    func setupUi() {
        self.configureCustomSearchController()
        self.title = "Make payment"
        
        plusButton.layer.borderColor = UIColor.gray.cgColor
        plusButton.layer.borderWidth = 1
        plusButton.layer.cornerRadius = 25
        
        plusButton.clipsToBounds = true
        plusButton.layer.cornerRadius = CGFloat(plusButton.frame.height / 2)
    }
    
    func configureCustomSearchController() {
        self.searchBar = CustomSearchBar(searchBarFrame: CGRect(x: 0.0,y: 0.0, width: tblView.frame.size.width, height: 56.0), searchBarFont: UIFont(name: Constants.MAIN_FONT_FAMILY.rawValue, size: 22.0)!, searchBarTextColor: UIColor.fromHex(HexColors.PRIMARY_COLOR.rawValue), searchBarTintColor: UIColor.fromHex(HexColors.WHITE_COLOR.rawValue), placeholderText: "Search")
        self.searchBar.delegate = self
        self.searchPlaceHolder.addSubview(searchBar)
    }
}

