//
//  PaymentTypesViewController.swift
//  Promize
//
//  Created by Isuru on 11/20/18.
//  Copyright © 2018 Creative Solutions. All rights reserved.
//

import UIKit

class PaymentTypesViewController: BaseViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var tblView: UITableView!

    var dataArray : [String] = ["Electercity","Water", "Internet", "Telephone", "Telivision", "Other payments"]
    let HEIGHT_OF_ROW = 85
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Choose payment type"
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(false)
        
        //        dataArray = SenzDb.instance.getIgifts(myGifts: true)
        
        if dataArray.count == 0 {
            tblView.isHidden = true
        }
        
        reloadTable()
    }
    
    func reloadTable() {
        DispatchQueue.main.async {
            self.tblView.reloadData()
        }
    }
    
    // MARK: UITableView Delegate Methods
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        // Try to find reusable cell
        var cell = tableView.dequeueReusableCell(withIdentifier: "PaymentTypesTableCell") as? PaymentTypesTableCell
        
        // If not available instantiate new custom cell
        if (cell == nil) {
            var nibArray = NSArray()
            nibArray = Bundle.main.loadNibNamed("PaymentTypesTableCell", owner: self, options: nil)! as NSArray
            cell = nibArray.object(at: 0) as? PaymentTypesTableCell
        }
        
        let data = dataArray[indexPath.row]
        cell?.nameLabel.text = data
        
        cell?.selectionStyle = .none
        
        return cell!
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return CGFloat(HEIGHT_OF_ROW)
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //        let vc = SendPromizeViewController(nibName: "SendPromizeViewController", bundle: nil)
        //        vc.selectedAmount =
        //        self.navigationController?.pushViewController(showGiftViewController, animated: false)
    }

}
