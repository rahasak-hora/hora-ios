//
//  AddWatchViewController.swift
//  Promize
//
//  Created by Isuru on 3/10/19.
//  Copyright © 2019 Creative Solutions. All rights reserved.
//

import UIKit

class AddWatchViewController: BaseViewController {

    @IBOutlet weak var detailsView: UIView!
    
    @IBOutlet weak var watchImageView: UIImageView!
    
    @IBOutlet weak var referenceText: UITextField!
    @IBOutlet weak var caseMaterialText: UITextField!
    @IBOutlet weak var caseBackText: UITextField!
    @IBOutlet weak var dialColorText: UITextField!
    @IBOutlet weak var crystalText: UITextField!
    @IBOutlet weak var resistanceText: UITextField!
    @IBOutlet weak var movementTypeText: UITextField!
    @IBOutlet weak var functionText: UITextField!
    
    @IBOutlet weak var doneButton: UIButton!
    @IBOutlet weak var saveButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setupUI()
    }

    private func setupUI(){
        watchImageView.layer.borderColor = UIColor.gray.cgColor
        watchImageView.layer.borderWidth = 1
        watchImageView.clipsToBounds = true
        watchImageView.layer.cornerRadius = CGFloat(watchImageView.frame.height / 2)
        
        detailsView.clipsToBounds = true
        detailsView.layer.cornerRadius = 10
        
        watchImageView.image = UIImage(named: "watch1") // remove this after set API
        
        // setup Done, Save button
        doneButton.backgroundColor = UIColor.clear
        doneButton.layer.cornerRadius = 8.0
        doneButton.layer.masksToBounds = true
        doneButton.layer.borderWidth = 1
        doneButton.layer.borderColor = UIColor.white.cgColor
        
        saveButton.backgroundColor = UIColor.clear
        saveButton.layer.cornerRadius = 8.0
        saveButton.layer.masksToBounds = true
        saveButton.layer.borderWidth = 1
        saveButton.layer.borderColor = UIColor.white.cgColor
        
        
    }
    
    @IBAction func tappedSaveButton(_ sender: Any) {
        
        self.dismiss(animated: true) {
            
        }
    }
    
    @IBAction func tappedDoneButton(_ sender: Any) {
        
        self.dismiss(animated: true) {
            
        }
    }
}
