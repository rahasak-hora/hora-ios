//
//  ChequeViewController.swift
//  Promize
//
//  Created by Isuru on 12/5/18.
//  Copyright © 2018 Creative Solutions. All rights reserved.
//

import UIKit

class ChequeViewController: BaseViewController, AlertViewControllerDelegate {


    var chequeImage: UIImage!
    var user: User!
    var selectedCheque: Cheque!
    
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var sendButton: UIButton!
    @IBOutlet weak var chequeImageView: UIImageView!
    @IBOutlet weak var sendGiftButton: UIButton!
    
    @IBOutlet weak var chequeView: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()

        self.isNavBarHidden = true
        
        chequeImageView.image = chequeImage//.rotate(radians: (.pi/2)*3)
//        self.chequeImageView.transform = CGAffineTransform(rotationAngle: CGFloat(Double.pi/2))
//        chequeImageView.layoutIfNeeded()
//        UIView.animate(withDuration:2.0, animations: {
//            self.chequeImageView.transform = CGAffineTransform(rotationAngle: .pi/2)
//        })
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        (UIApplication.shared.delegate as! AppDelegate).deviceOrientation = .landscapeLeft
        let value = UIInterfaceOrientation.landscapeLeft.rawValue
        UIDevice.current.setValue(value, forKey: "orientation")

    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)

        (UIApplication.shared.delegate as! AppDelegate).deviceOrientation = .portrait
        let value = UIInterfaceOrientation.portrait.rawValue
        UIDevice.current.setValue(value, forKey: "orientation")
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func tappedOnCancelButton(_ sender: UIButton) {
        goBack(animated: false)
    }
    
    @IBAction func tappedOnSendButton(_ sender: UIButton) {
        giftSendConfirmation(amount: selectedCheque.amount)
    }
    
    func giftSendConfirmation(amount: String) {
        var enteredPassword:String = ""
        let alertController = UIAlertController(title: "Enter Password", message: "", preferredStyle: .alert)
        let saveAction = UIAlertAction(title: "Done", style: .default, handler: { alert -> Void in
            let savedPassword = PreferenceUtil.instance.get(key: PreferenceUtil.PASSWORD)
            enteredPassword = alertController.textFields![0].text!
            if savedPassword == enteredPassword {
                self.sendIgift(amount: amount)
            } else {
                ViewControllerUtil.showAlert(alertTitle: "Error", alertMessage: "Invalid password")
            }
        })
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .default, handler: { (action : UIAlertAction!) -> Void in })
        alertController.addTextField { (textField : UITextField!) -> Void in
            textField.isSecureTextEntry = true
        }
        
        alertController.addAction(saveAction)
        alertController.addAction(cancelAction)
        
        self.present(alertController, animated: true, completion: nil)
    }
    
    func sendIgift(amount: String) {
        let imgData = self.captureScreen()
        let blob = imgData.base64EncodedString()
        let cid = NSUUID().uuidString
        let uid = SenzUtil.instance.uid(zAddress: PreferenceUtil.instance.get(key: PreferenceUtil.PHONE_NUMBER))
        let senz = SenzUtil.instance.transferSenz(amount: amount, blob: blob, to: self.user!.phone, cid: cid)
        
        // send request
        SenzProgressView.shared.showProgressView((self.navigationController?.view)!)
        DispatchQueue.global(qos: .userInitiated).async {
            // post to contractz
            let dict = ["Uid": uid, "Msg": senz]
            Httpz.instance.doPost(param: dict, onComplete: {(senzes: [Senz]) -> Void in
                if (senzes.count > 0 && senzes.first!.attr["#status"] == "200") {
                    // save igift
                    self.saveIGift(uid: uid, cid: cid, amount: amount, data: imgData)
                    
                    DispatchQueue.main.async {
                        // exit view controller
                        SenzProgressView.shared.hideProgressView()
                        
                        let viewContUtil = ViewControllerUtil()
                        viewContUtil.delegate = self
                        viewContUtil.showAlertWithSingleActions(alertTitle: "Success", alertMessage: "Successfully sent Promize", viewController: self)
                    }
                } else {
                    // fail
                    DispatchQueue.main.async {
                        // fail to send Promize
                        SenzProgressView.shared.hideProgressView()
                        ViewControllerUtil.showAlert(alertTitle: "Error", alertMessage: "Fail to send Promize")
                    }
                }
            })
        }
    }
    
    func captureScreen() -> NSData {
        
        let screenshot = chequeView.takeSnapshot().rotate(radians: -(.pi/2))
        let compressedImageData = screenshot.lowQualityJPEGNSData
        
        return compressedImageData
    }
    
    func saveIGift(uid: String, cid: String, amount: String, data: NSData) {
        // save image
        let filename = uid + ".jpeg"
        _ = createFileInPath(relativeFilePath: Constants.IMAGES_DIR.rawValue, fileName: filename, imageData: data as Data)
        
        // save Promize
        let ig = Igift(id: 1)
        ig.uid = uid
        ig.amount = amount
        ig.account = PreferenceUtil.instance.get(key: PreferenceUtil.ACCOUNT)
        ig.state = "TRANSFER"
        ig.isMyIgift = true
        ig.cid = cid
        ig.user = user!.phone
        ig.timestamp = TimeUtil.sharedInstance.timestamp()
        _ = SenzDb.instance.createIgift(igift: ig)
    }
    
    func executeTaskForAction(actionTitle: String) {
        if actionTitle == "OK" {
            DispatchQueue.main.async {
                // exit view controller
                self.navigationController?.popViewController(animated: false)
//                self.navigationController?.popToRootViewController(animated: false)
            }
        }
    }
}
