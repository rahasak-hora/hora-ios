//
//  ChequeDetailsViewController.swift
//  Promize
//
//  Created by Isuru on 1/9/19.
//  Copyright © 2019 Creative Solutions. All rights reserved.
//

import UIKit

struct Cheque {
    var date: String
    var amount: String
    var userName: String
}


class ChequeDetailsViewController : KeyboardScrollableViewController {
    
    @IBOutlet weak var txtFieldCustomerName: UITextField!
    @IBOutlet weak var txtFieldAmount: UITextField!
    @IBOutlet weak var txtFieldDate: UITextField!
    
    @IBOutlet weak var btnSign: UIButton!
    
    var user: User!
    
    fileprivate let dateFormate = "dd/MM/yyyy"
    let datePicker = UIDatePicker()
    lazy var datePickerToolbar:UIToolbar = {
        
        //ToolBar
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(doneDatePicker));
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelDatePicker));
        
        toolbar.setItems([doneButton,spaceButton,cancelButton], animated: false)
        return toolbar
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupUi()
    }
    
    func setupUi() {
        self.title = "To:\(user.zid)"
        self.setupStylesForTextFields()
        showDatePicker(sender: txtFieldDate)
    }
    
    func setupStylesForTextFields(){
        UITextField.applyStyle(txtField: self.txtFieldCustomerName)
        UITextField.applyStyle(txtField: self.txtFieldAmount)
        UITextField.applyStyle(txtField: self.txtFieldDate)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.setupStylesForTextFields()
    }
    
    @IBAction func tappedOnSignButton(_ sender: Any) {
        
        if txtFieldCustomerName.text?.isEmpty ?? false{
            ViewControllerUtil.showAlert(alertTitle: "Notice", alertMessage: "Mandatory field can't be empty.")
            
            return
        }
        
        if txtFieldAmount.text?.isEmpty ?? false{
            ViewControllerUtil.showAlert(alertTitle: "Notice", alertMessage: "Mandatory field can't be empty.")
            
            return
        }
        
        if txtFieldDate.text?.isEmpty ?? false{
            ViewControllerUtil.showAlert(alertTitle: "Notice", alertMessage: "Mandatory field can't be empty.")
            
            return
        }
        
        let cheque = Cheque(date: txtFieldDate.text ?? "", amount: txtFieldAmount.text ?? "", userName: txtFieldCustomerName.text ?? "")
        let returnImage = ViewControllerUtil.textToImage(cheque: cheque, inImage: UIImage(named: "cheque")!)
        
        let chequeViewController = ChequeViewController(nibName: "ChequeViewController", bundle: nil)
        chequeViewController.user = user
        chequeViewController.selectedCheque = cheque
        chequeViewController.chequeImage = returnImage
        self.navigationController?.pushViewController(chequeViewController, animated: true)
    }
    
    
    func showDatePicker(sender: UITextField){
        //Formate Date
        datePicker.datePickerMode = .date
        
        let formatter = DateFormatter()
        formatter.dateFormat = dateFormate
        formatter.locale = Locale(identifier: "en_US_POSIX")
        sender.text = formatter.string(from: datePicker.date)
        
        let selectedDate = formatter.date(from: sender.text ?? "")
        if let selectedDate = selectedDate{
            datePicker.date = selectedDate
        }else{
            datePicker.date = Date()
        }
        
        sender.inputAccessoryView = datePickerToolbar
        sender.inputView = datePicker
        
    }
    
    @objc func doneDatePicker(){
        
        let formatter = DateFormatter()
        formatter.dateFormat = dateFormate
        formatter.locale = Locale(identifier: "en_US_POSIX")
        txtFieldDate.text = formatter.string(from: datePicker.date)
        self.view.endEditing(true)
    }
    
    @objc func cancelDatePicker(){
        self.view.endEditing(true)
    }
    

}
