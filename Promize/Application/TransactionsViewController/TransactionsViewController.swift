//
//  TransactionsViewController.swift
//  Promize
//
//  Created by Isuru on 11/14/18.
//  Copyright © 2018 Creative Solutions. All rights reserved.
//

import UIKit

class TransactionsViewController: BaseViewController {

    @IBOutlet weak var segmentedControl: CustomSegmentedControl!
    
    @IBOutlet weak var topTableViewConstraint: NSLayoutConstraint!
    
    private var pageController: UIPageViewController!
    private var arrVC:[UIViewController] = []
    private var currentPage: Int!
    
    /// Instantiate ViewControllers Here With Lazy Keyword
    
    lazy var vc1: TransactionsListViewController = {
        let storyboard = UIStoryboard(name: "TransactionsWorkFlow", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "TransactionsListViewController") as! TransactionsListViewController
        vc.title = "Deposits"
        vc.parentView = self
        return vc
    }()
    
    lazy var vc2: TransactionsListViewController = {
        let storyboard = UIStoryboard(name: "TransactionsWorkFlow", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "TransactionsListViewController") as! TransactionsListViewController
        vc.title = "Withdrawals"
        vc.parentView = self
        return vc
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        segmentedControl.commaSeperatedButtonTitles = "Deposits, Withdrawals"
        segmentedControl.addTarget(self, action: #selector(onChangeOfSegment(_:)), for: .valueChanged)
        
        super.viewDidLoad()
        
        currentPage = 0
        arrVC.append(vc1)
        arrVC.append(vc2)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        topTableViewConstraint.constant = -120
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "TransactionPageViewControllerSegue" {
            self.pageController = segue.destination as! UIPageViewController
            
            self.pageController.view.backgroundColor = UIColor.clear
            self.pageController.delegate = self
            self.pageController.dataSource = self
            
            for svScroll in self.pageController.view.subviews as! [UIScrollView] {
                svScroll.delegate = self
            }
            self.pageController.setViewControllers([vc1], direction: UIPageViewControllerNavigationDirection.forward, animated: false, completion: nil)
            self.title = "Transactions"
        }
    }

}

extension TransactionsViewController: UIPageViewControllerDataSource, UIPageViewControllerDelegate, UIScrollViewDelegate
{
    private func indexofviewController(viewCOntroller: UIViewController) -> Int {
        if(arrVC .contains(viewCOntroller)) {
            return arrVC.index(of: viewCOntroller)!
        }
        
        return -1
    }
    
    
    //MARK: - Pagination Delegate Methods
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        
        var index = indexofviewController(viewCOntroller: viewController)
        
        if(index != -1) {
            index = index - 1
        }
        
        if(index < 0) {
            return nil
        }
        else {
            return arrVC[index]
        }
        
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        
        var index = indexofviewController(viewCOntroller: viewController)
        
        if(index != -1) {
            index = index + 1
        }
        
        if(index >= arrVC.count) {
            return nil
        }
        else {
            return arrVC[index]
        }
        
    }
    
    func pageViewController(_ pageViewController1: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        
        if(completed) {
            currentPage = arrVC.index(of: (pageViewController1.viewControllers?.last)!)
            // self.segmentedControl.selectedSegmentIndex = currentPage
            
            self.segmentedControl.updateSegmentedControlSegs(index: currentPage)
            
        }
        
    }
    
    @objc func onChangeOfSegment(_ sender: CustomSegmentedControl) {
        
        
        switch sender.selectedSegmentIndex {
        case 0:
            let vc = arrVC[0]
            pageController.setViewControllers([vc], direction: UIPageViewControllerNavigationDirection.reverse, animated: true, completion: nil)
            currentPage = 0
        //            self.title = vc.title
        case 1:
            let vc = arrVC[1]
            if currentPage > 1{
                pageController.setViewControllers([vc], direction: UIPageViewControllerNavigationDirection.reverse, animated: true, completion: nil)
                currentPage = 1
            }else{
                pageController.setViewControllers([vc], direction: UIPageViewControllerNavigationDirection.forward, animated: true, completion: nil)
                currentPage = 1
                
            }
        //            self.title = vc.title
        default:
            break
        }
        
        
    }
    
}
