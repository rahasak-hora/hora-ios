//
//  TransferFundsTableViewCell.swift
//  Promize
//
//  Created by Arosha Piyadigama on 11/16/18.
//  Copyright © 2018 AnujAroshA. All rights reserved.
//

import UIKit

class TransferFundsTableViewCell: UITableViewCell {
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var accountNumberLabel: UILabel!
    @IBOutlet weak var bankNameLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
