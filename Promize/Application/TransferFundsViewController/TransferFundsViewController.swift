////
////  TransferFundsViewController.swift
////  Promize
////
////  Created by Arosha Piyadigama on 11/16/18.
////  Copyright © 2018 Creative Solutions. All rights reserved.
////
//
//import UIKit
//
//class TransferFundsViewController:BaseViewController, UITableViewDelegate, UITableViewDataSource, UISearchResultsUpdating, UISearchBarDelegate {
//    
//    @IBOutlet weak var tblView: UITableView!
//    @IBOutlet weak var searchPlaceHolder: UIView!
//    @IBOutlet weak var plusButton: UIButton!
//    
//    var searchBar: CustomSearchBar!
//    let HEIGHT_OF_ROW = 70
//    var accountsArray = [Account]()
//    var filteredArray = [Account]()
//    var shouldShowSearchResults = false
//    
//    //    MARK: UIViewController lifecycle
//    override func viewDidLoad() {
//        super.viewDidLoad()
//        
//        self.setupUi()
//        
//        accountsArray = SenzDb.instance.getAccounts()
//        
//        if accountsArray.count == 0 {
//            tblView.isHidden = true
//        }
//    }
//    
//    // MARK: UITableViewDataSource
//    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        if shouldShowSearchResults {
//            return filteredArray.count
//        }
//        else {
//            return accountsArray.count
//        }
//    }
//
//    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//
//        // Try to find reusable cell
//        var cell = tableView.dequeueReusableCell(withIdentifier: "TransferFundsTableViewCell") as? TransferFundsTableViewCell
//
//        // If not available instantiate new custom cell
//        if (cell == nil) {
//            var nibArray = NSArray()
//            nibArray = Bundle.main.loadNibNamed("TransferFundsTableViewCell", owner: self, options: nil)! as NSArray
//            cell = nibArray.object(at: 0) as? TransferFundsTableViewCell
//        }
//
//        // Setup Cells in table
//        cell?.nameLabel?.text = accountsArray[indexPath.row].user
//        cell?.accountNumberLabel?.text = accountsArray[indexPath.row].no
//        cell?.bankNameLabel.text = accountsArray[indexPath.row].bank
//
//        cell?.selectionStyle = .none
//        return cell!
//    }
//
//    // MARK: UITableViewDelegate
//    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        return CGFloat(HEIGHT_OF_ROW)
//    }
//
//    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//
//    }
//
//    // MARK: UISearchResultsUpdating
//    func updateSearchResults(for searchController: UISearchController) {
//
//    }
//    
//    // MARK: UISearchBarDelegate
//    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
//        shouldShowSearchResults = true
//        filteredArray.removeAll()
//        filteredArray = accountsArray
//        self.reloadTable()
//    }
//    
//    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
//        shouldShowSearchResults = false
//        self.searchBar.text = ""
//        searchBar.resignFirstResponder()
//        self.reloadTable()
//    }
//    
//    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
//        if !shouldShowSearchResults {
//            shouldShowSearchResults = true
//            self.reloadTable()
//        }
//    }
//    
//    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
//        if searchText.isEmpty {
//            filteredArray.removeAll()
//            filteredArray = accountsArray
//        }
//        else {
//            filteredArray = accountsArray.filter({ (accounts) -> Bool in
//                return accounts.user.lowercased().range(of:searchText.lowercased()) != nil
//            })
//        }
//        
//        self.reloadTable()
//    }
//    
//    //    MARK: Action methods
//    @IBAction func plusButtonAction(_ sender: UIButton) {
//        let bankListViewController = BankListViewController(nibName: "BankListViewController", bundle: nil)
//        self.navigationController?.pushViewController(bankListViewController, animated: false)
//    }
//    
//    //    MARK: Supportive methods
//    func setupUi() {
//        self.configureCustomSearchController()
//        self.title = "Transfer Funds"
//        
//        plusButton.layer.borderColor = UIColor.gray.cgColor
//        plusButton.layer.borderWidth = 1
//        plusButton.layer.cornerRadius = 25
//        
//        plusButton.clipsToBounds = true
//        plusButton.layer.cornerRadius = CGFloat(plusButton.frame.height / 2)
//    }
//    
//    func configureCustomSearchController() {
//        self.searchBar = CustomSearchBar(searchBarFrame: CGRect(x: 0.0,y: 0.0, width: tblView.frame.size.width, height: 56.0), searchBarFont: UIFont(name: Constants.MAIN_FONT_FAMILY.rawValue, size: 22.0)!, searchBarTextColor: UIColor.fromHex(HexColors.PRIMARY_COLOR.rawValue), searchBarTintColor: UIColor.fromHex(HexColors.WHITE_COLOR.rawValue), placeholderText: "Search")
//        self.searchBar.delegate = self
//        self.searchPlaceHolder.addSubview(searchBar)
//    }
//    
//    func reloadTable() {
//        DispatchQueue.main.async {
//            self.tblView.reloadData()
//        }
//    }
//}
//
