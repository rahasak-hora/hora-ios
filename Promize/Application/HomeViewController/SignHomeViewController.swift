//
//  SignHomeViewController.swift
//  Promize
//
//  Created by Isuru on 2/8/19.
//  Copyright © 2019 Creative Solutions. All rights reserved.
//

import UIKit

class SignHomeViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    @IBAction func tappedOnDocumentStatusButtons(_ sender: UITapGestureRecognizer) {
        var docfilterType = DocFilterType.all
        switch sender.view?.tag {
        case 101:
            docfilterType = DocFilterType.pending
        case 102:
            docfilterType = DocFilterType.inProgress
        case 103:
            docfilterType = DocFilterType.rejected
        case 104:
            docfilterType = DocFilterType.compeleted
        case 100:
            docfilterType = DocFilterType.all
        default:
            docfilterType = DocFilterType.all
        }
        let viewC = WatchesListViewController(nibName: "WatchesListViewController", bundle: nil)
        self.navigationController?.pushViewController(viewC, animated: true)
    }
    
//    @IBAction func tappedOnAllDocumentsButton(_ sender: Any) {
//        let viewC = WatchesListViewController(nibName: "WatchesListViewController", bundle: nil)
//        viewC.docfilterType = DocFilterType.all
//        self.navigationController?.pushViewController(viewC, animated: true)
//        
//    }


}
