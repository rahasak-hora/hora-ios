//
//  PromizeHomeViewController.swift
//  Promize
//
//  Created by Arosha Piyadigama on 10/30/18.
//  Copyright © 2018 AnujAroshA. All rights reserved.
//

import UIKit

class PromizeHomeViewController: BaseViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var tableView: UITableView!
    var promizeCategories: Array<String>!
    var cellSubtitles: Array<String>!
    let cellReusableIdentifier = "PromizeCategoriesCell"

    //    MARK: UIViewController lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()

        self.setNavBarHidden(true)
        
        promizeCategories = ["Send proniZe", "Customers", "Received promiZes", "Sent promiZes", "Settings"];
        cellSubtitles = ["Transfer money with promize",
                         "Connected promize customers",
                         "promiZe you have received",
                         "promiZe you have sent",
                         "Setup your app preferences"];
    }
    
    //    MARK : UITableViewDataSource
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return promizeCategories.count
    }
 
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        var cell = tableView.dequeueReusableCell(withIdentifier: cellReusableIdentifier) as? HomeTableViewCell
        
        if cell == nil {
            var nibArray = NSArray()
            nibArray = Bundle.main.loadNibNamed("HomeTableViewCell", owner: self, options: nil)! as NSArray
            cell = nibArray.object(at: 0) as? HomeTableViewCell
        }

        cell?.mainLabel?.text = promizeCategories[indexPath.row]
        cell?.detailLabel?.text = cellSubtitles[indexPath.row];
        
        switch indexPath.row {
        case 0:
            cell?.cellImageView.image = UIImage(imageLiteralResourceName: "promize")
        case 1:
            cell?.cellImageView.image = UIImage(imageLiteralResourceName: "transactions")
        case 2:
            cell?.cellImageView.image = UIImage(imageLiteralResourceName: "fund transfer")
        case 3:
            cell?.cellImageView.image = UIImage(imageLiteralResourceName: "payments")
        case 4:
            cell?.cellImageView.image = UIImage(imageLiteralResourceName: "settings")
        default:
            cell?.cellImageView.image = UIImage(imageLiteralResourceName: "default_user")
        }
        
        return cell!
    }
    
    //    MARK : UITableViewDelegate
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return tableView.bounds.height/CGFloat(promizeCategories.count)
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return indexPath.row == 0 ? true : false
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {

        let scan = UITableViewRowAction(style: .normal, title: "Scan") { action, index in
            print("scan button tapped")
        }
        scan.backgroundColor = .cyan
        
        let send = UITableViewRowAction(style: .normal, title: "Send") { action, index in
            print("Send button tapped")
        }
        send.backgroundColor = .blue
        
        return [scan, send]
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.row {
            case 0:
                if (PreferenceUtil.instance.get(key: PreferenceUtil.ACCOUNT).isEmpty) {
                    // no account
                    // verified account
                    //            self.loadView("AddAccountInfoViewController")
                    let addAccountInfoViewController = AddAccountInfoViewController(nibName: "AddAccountInfoViewController", bundle: nil)
                    self.navigationController?.pushViewController(addAccountInfoViewController, animated: true)
                } else {
                    // have account
                    if (PreferenceUtil.instance.get(key: PreferenceUtil.ACCOUNT_STATUS) == "VERIFIED") {
                        // verified account
                        if (SenzDb.instance.hasUsers()) {
                            let contactsViewController = ContactsViewController(nibName: "ContactsViewController", bundle: nil)
                            contactsViewController.forNewIgift = true
                            self.navigationController?.pushViewController(contactsViewController, animated: true)
                        } else {
                            let noContactViewController = NoContactViewController(nibName: "NoContactViewController", bundle: nil)
                            self.navigationController?.pushViewController(noContactViewController, animated: true)
                        }
                    }
                    else {
                        // not verified account
                        //                self.loadView("ConfirmAccountViewController")
                        let confirmAccountViewController = ConfirmAccountViewController(nibName: "ConfirmAccountViewController", bundle: nil)
                        self.navigationController?.pushViewController(confirmAccountViewController, animated: true)
                    }
            }
            
            case 1:
                let contactsViewController = ContactsViewController(nibName: "ContactsViewController", bundle: nil)
                contactsViewController.forNewIgift = false
                self.navigationController?.pushViewController(contactsViewController, animated: true)
            case 2:
                let receivedViewController = IGiftsReceivedViewController(nibName: "IGiftsReceivedViewController", bundle: nil)
                self.navigationController?.pushViewController(receivedViewController, animated: true)
            case 3:
                let sentViewController = IGiftsSentViewController(nibName: "IGiftsSentViewController", bundle: nil)
                self.navigationController?.pushViewController(sentViewController, animated: true)
            case 4:
                let sentViewController = IGiftsSentViewController(nibName: "IGiftsSentViewController", bundle: nil)
                self.navigationController?.pushViewController(sentViewController, animated: true)
            case 5:
                self.loadView("SettingsViewController")
        default:
            ()
        }
    }
}
