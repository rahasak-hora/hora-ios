//
//  PromizeAmountTableViewCell.swift
//  Promize
//
//  Created by Isuru on 11/6/18.
//  Copyright © 2018 Creative Solutions. All rights reserved.
//

import UIKit

class PromizeAmountTableViewCell: UITableViewCell {

    @IBOutlet weak var amountLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
