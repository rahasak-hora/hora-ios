//
//  PromizeSelectionViewController.swift
//  Promize
//
//  Created by Isuru on 11/6/18.
//  Copyright © 2018 Creative Solutions. All rights reserved.
//

import UIKit

class PromizeSelectionViewController: BaseViewController , UITableViewDelegate, UITableViewDataSource{

    @IBOutlet weak var tblView: UITableView!
    
    var dataArray = [500, 1000, 2000, 5000, 10000]
    let HEIGHT_OF_ROW = 85
    
    override func viewDidLoad() {
        super.viewDidLoad()

        title = "Choose amount"
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(false)
        
        reloadTable()
    }

    func reloadTable() {
        DispatchQueue.main.async {
            self.tblView.reloadData()
        }
    }
    
    // MARK: UITableView Delegate Methods
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataArray.count + 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        // Try to find reusable cell
        var cell = tableView.dequeueReusableCell(withIdentifier: "PromizeAmountTableViewCell") as? PromizeAmountTableViewCell
        
        // If not available instantiate new custom cell
        if (cell == nil) {
            var nibArray = NSArray()
            nibArray = Bundle.main.loadNibNamed("PromizeAmountTableViewCell", owner: self, options: nil)! as NSArray
            cell = nibArray.object(at: 0) as? PromizeAmountTableViewCell
        }
        
        if dataArray.count == indexPath.row{
            cell?.amountLabel.text = "Other amount"
        }else{
            let data = dataArray[indexPath.row]
            cell?.amountLabel.text = "Rs \(data)"
        }
        
        
        cell?.selectionStyle = .none
        
        return cell!
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return CGFloat(HEIGHT_OF_ROW)
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        if dataArray.count == indexPath.row{
            
        }else{
            let vc = SendPromizeViewController(nibName: "SendPromizeViewController", bundle: nil)
            vc.selectedAmount = dataArray[indexPath.row]
            self.navigationController?.pushViewController(vc, animated: false)
        }
        
        
        
    }


}
