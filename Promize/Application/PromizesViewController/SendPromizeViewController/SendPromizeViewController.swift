//
//  SendPromizeViewController.swift
//  Promize
//
//  Created by Isuru on 10/26/18.
//  Copyright © 2018 Creative Solutions. All rights reserved.
//

import UIKit

//barcode scaner
class SendPromizeViewController: BaseViewController {

    @IBOutlet weak var qrcodeImageView: UIImageView!
    
    var selectedAmount: Int!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Send promiZe"
        setupBarCode()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    private func setupBarCode(){
        if let image = generateQRCode(from: "Check this out- :) Eranga"){
            qrcodeImageView.image = image
        }else{
            
        }
        
    }

    func generateQRCode(from string: String) -> UIImage? {
        let data = string.data(using: String.Encoding.ascii)
        
        if let filter = CIFilter(name: "CIQRCodeGenerator") {
            filter.setValue(data, forKey: "inputMessage")
            let transform = CGAffineTransform(scaleX: 3, y: 3)
            
            if let output = filter.outputImage?.transformed(by: transform) {
                return UIImage(ciImage: output)
            }
        }
        
        return nil
    }
    
    

}
