//
//  PromizeListViewController.swift
//  Promize
//
//  Created by Isuru on 11/6/18.
//  Copyright © 2018 Creative Solutions. All rights reserved.
//

import UIKit

class PromizeListViewController: UIViewController , UITableViewDelegate, UITableViewDataSource{

    @IBOutlet weak var tblView: UITableView!
    
    var dataArray = [Igift]()
    let HEIGHT_OF_ROW = 85
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let x1 = Igift(id: 1)
        x1.account = "test1"
        x1.user = "Isur"
        x1.timestamp = 1213213
        x1.amount = "1000"
        
        let x2 = Igift(id: 2)
        x2.account = "test2"
        x2.user = "Eranga"
        x2.timestamp = 1213213
        x2.amount = "2000"
        
        let x3 = Igift(id: 2)
        x3.account = "test3"
        x3.user = "Arosha"
        x3.timestamp = 1213213
        x3.amount = "4000"
        
        dataArray.append(x1)
        dataArray.append(x2)
        dataArray.append(x3)
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(false)

//        dataArray = SenzDb.instance.getIgifts(myGifts: true)
        
        if dataArray.count == 0 {
            tblView.isHidden = true
        }
        
        reloadTable()
    }
    
    func reloadTable() {
        DispatchQueue.main.async {
            self.tblView.reloadData()
        }
    }
    
    // MARK: UITableView Delegate Methods
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        // Try to find reusable cell
        var cell = tableView.dequeueReusableCell(withIdentifier: "CustomIGiftCell") as? CustomIGiftCell
        
        // If not available instantiate new custom cell
        if (cell == nil) {
            var nibArray = NSArray()
            nibArray = Bundle.main.loadNibNamed("CustomIGiftCell", owner: self, options: nil)! as NSArray
            cell = nibArray.object(at: 0) as? CustomIGiftCell
        }
        
        let data = dataArray[indexPath.row]
        cell?.setAccountNo(data.account)
        cell?.lblName?.text = data.user //PhoneBook.instance.getContact(phone: data.user)?.name
        cell?.lblTime?.text = TimeUtil.sharedInstance.timeAgoSinceDate(data.timestamp/1000)
        cell?.lblAmount?.text = data.amount + ".00"
        
        cell?.selectionStyle = .none
        
        return cell!
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return CGFloat(HEIGHT_OF_ROW)
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        let vc = SendPromizeViewController(nibName: "SendPromizeViewController", bundle: nil)
//        vc.selectedAmount =
//        self.navigationController?.pushViewController(showGiftViewController, animated: false)
    }

}
