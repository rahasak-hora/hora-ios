//
//  PromizesViewController.swift
//  Promize
//
//  Created by Isuru on 11/6/18.
//  Copyright © 2018 Creative Solutions. All rights reserved.
//

import UIKit
import BarcodeScanner

class PromizesViewController: BaseViewController {

    @IBOutlet weak var segmentedControl: CustomSegmentedControl!
    
    @IBOutlet weak var bcScanerButton: UIButton!
    @IBOutlet weak var sendPButton: UIButton!
    
    private var pageController: UIPageViewController!
    private var arrVC:[UIViewController] = []
    private var currentPage: Int!
    
    /// Instantiate ViewControllers Here With Lazy Keyword
    
    lazy var vc1: PromizeListViewController = {
        let storyboard = UIStoryboard(name: "PromizesWorkFlow", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "PromizeListViewController") as! PromizeListViewController
        vc.title = "Received"
        return vc
    }()
    
    lazy var vc2: PromizeListViewController = {
        let storyboard = UIStoryboard(name: "PromizesWorkFlow", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "PromizeListViewController") as! PromizeListViewController
        vc.title = "Sent"
        
        return vc
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        bcScanerButton.layer.borderColor = UIColor.gray.cgColor
        bcScanerButton.layer.borderWidth = 1
        bcScanerButton.layer.cornerRadius = 25
        
        sendPButton.layer.borderColor = UIColor.gray.cgColor
        sendPButton.layer.borderWidth = 1
        sendPButton.layer.cornerRadius = 25
        
        segmentedControl.commaSeperatedButtonTitles = "Received, Sent"
        segmentedControl.addTarget(self, action: #selector(onChangeOfSegment(_:)), for: .valueChanged)
        
        super.viewDidLoad()
        
        currentPage = 0
        arrVC.append(vc1)
        arrVC.append(vc2)
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "PageViewControllerSegue" {
            self.pageController = segue.destination as! UIPageViewController
            
            self.pageController.view.backgroundColor = UIColor.clear
            self.pageController.delegate = self
            self.pageController.dataSource = self
            
            for svScroll in self.pageController.view.subviews as! [UIScrollView] {
                svScroll.delegate = self
            }
            self.pageController.setViewControllers([vc1], direction: UIPageViewControllerNavigationDirection.forward, animated: false, completion: nil)
            self.title = "Promizes"
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    
    @IBAction func tappedOnGetPromizeButton(_ sender: Any) {
        
        let viewController = BarcodeScannerViewController()
        viewController.messageViewController.messages.processingText = "Looking for your ..."
        viewController.codeDelegate = self
        viewController.errorDelegate = self
        viewController.dismissalDelegate = self
        
        present(viewController, animated: true, completion: nil)
        
    }
    
    @IBAction func tappedOnSendPeomizeButton(_ sender: Any) {
        
        let view = PromizeSelectionViewController(nibName: "PromizeSelectionViewController", bundle: nil)
        self.navigationController?.pushViewController(view, animated: true)
    }
    
}

extension PromizesViewController: UIPageViewControllerDataSource, UIPageViewControllerDelegate, UIScrollViewDelegate
{
    private func indexofviewController(viewCOntroller: UIViewController) -> Int {
        if(arrVC .contains(viewCOntroller)) {
            return arrVC.index(of: viewCOntroller)!
        }
        
        return -1
    }
    
    
    //MARK: - Pagination Delegate Methods
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        
        var index = indexofviewController(viewCOntroller: viewController)
        
        if(index != -1) {
            index = index - 1
        }
        
        if(index < 0) {
            return nil
        }
        else {
            return arrVC[index]
        }
        
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        
        var index = indexofviewController(viewCOntroller: viewController)
        
        if(index != -1) {
            index = index + 1
        }
        
        if(index >= arrVC.count) {
            return nil
        }
        else {
            return arrVC[index]
        }
        
    }
    
    func pageViewController(_ pageViewController1: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        
        if(completed) {
            currentPage = arrVC.index(of: (pageViewController1.viewControllers?.last)!)
            // self.segmentedControl.selectedSegmentIndex = currentPage
            
            self.segmentedControl.updateSegmentedControlSegs(index: currentPage)
            
        }
        
    }
    
    @objc func onChangeOfSegment(_ sender: CustomSegmentedControl) {
        
        
        switch sender.selectedSegmentIndex {
        case 0:
            let vc = arrVC[0]
            pageController.setViewControllers([vc], direction: UIPageViewControllerNavigationDirection.reverse, animated: true, completion: nil)
            currentPage = 0
//            self.title = vc.title
        case 1:
            let vc = arrVC[1]
            if currentPage > 1{
                pageController.setViewControllers([vc], direction: UIPageViewControllerNavigationDirection.reverse, animated: true, completion: nil)
                currentPage = 1
            }else{
                pageController.setViewControllers([vc], direction: UIPageViewControllerNavigationDirection.forward, animated: true, completion: nil)
                currentPage = 1
                
            }
//            self.title = vc.title
        default:
            break
        }
        
        
    }
    
}

// MARK: - BarcodeScannerCodeDelegate

extension PromizesViewController: BarcodeScannerCodeDelegate {
    func scanner(_ controller: BarcodeScannerViewController, didCaptureCode code: String, type: String) {
        print("Barcode Data: \(code)")
        print("Symbology Type: \(type)")
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
            //            controller.resetWithError(message: "testing")
            
            controller.dismiss(animated: false, completion: nil)
            
            let view = GetPromizeViewController(nibName: "GetPromizeViewController", bundle: nil)
            self.present(view, animated: true, completion: nil)
//            self.navigationController?.pushViewController(view, animated: true)
            

        }
    }
}

// MARK: - BarcodeScannerErrorDelegate

extension PromizesViewController: BarcodeScannerErrorDelegate {
    func scanner(_ controller: BarcodeScannerViewController, didReceiveError error: Error) {
        print(error)
    }
}

// MARK: - BarcodeScannerDismissalDelegate

extension PromizesViewController: BarcodeScannerDismissalDelegate {
    func scannerDidDismiss(_ controller: BarcodeScannerViewController) {
        controller.dismiss(animated: true, completion: nil)
    }
}


