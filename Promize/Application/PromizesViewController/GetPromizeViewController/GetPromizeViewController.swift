//
//  BarCodeReaderViewController.swift
//  Promize
//
//  Created by Isuru on 10/26/18.
//  Copyright © 2018 Creative Solutions. All rights reserved.
//

import UIKit
import BarcodeScanner

class GetPromizeViewController: BaseViewController {

    
    @IBOutlet weak var promizeDetailsLabel: UILabel!
    @IBOutlet weak var promizeConfirmLabel: UILabel!
    
    @IBOutlet weak var buttonView: UIStackView!
    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = "Send promiZe"
        
//        resetView()
//        loadBarCode()
        
        dispalyResult()
    }
    
    private func resetView(){
        promizeDetailsLabel.isHidden = true
        promizeConfirmLabel.isHidden = true
        buttonView.isHidden = true
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    private func dispalyResult(){
        promizeDetailsLabel.isHidden = false
        promizeConfirmLabel.isHidden = false
        buttonView.isHidden = false
        
        let amount = 1000
        let name = "Isuru"
        let accountNo = "422332243"
        
        promizeDetailsLabel.text = "You are about to recevie Rs \(amount) promiZe from \(name) (account - \(accountNo)"
        
    }
    
    
    @IBAction func tappedConfirmButton(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func tappedCancelButton(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
}


