//
//  WatchDetailsViewController.swift
//  Promize
//
//  Created by Isuru on 2/8/19.
//  Copyright © 2019 Creative Solutions. All rights reserved.
//

import UIKit
import MapKit

class WatchDetailsViewController: BaseViewController {

    var selectedWatch : Watch!
    
    @IBOutlet weak var detailsView: UIView!
    
    @IBOutlet weak var watchDescription: UILabel!
    @IBOutlet weak var watchImageView: UIImageView!
    
    @IBOutlet weak var referenceText: UILabel!
    @IBOutlet weak var caseMaterialText: UILabel!
    @IBOutlet weak var caseBackText: UILabel!
    @IBOutlet weak var dialColorText: UILabel!
    @IBOutlet weak var crystalText: UILabel!
    @IBOutlet weak var resistanceText: UILabel!
    @IBOutlet weak var movementTypeText: UILabel!
    @IBOutlet weak var functionText: UILabel!
    
    @IBOutlet weak var trackHistoryTableView: UITableView!
    @IBOutlet weak var tableHeight: NSLayoutConstraint!
    
    @IBOutlet weak var mapView: MKMapView!
    
    @IBOutlet weak var transferOwnershipButton: UIButton!
    var dataArray = [WatchHistory]()
    let HEIGHT_OF_ROW = 95
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = selectedWatch.model
        
        watchImageView.layer.borderColor = UIColor.gray.cgColor
        watchImageView.layer.borderWidth = 1
        watchImageView.clipsToBounds = true
        watchImageView.layer.cornerRadius = CGFloat(watchImageView.frame.height / 2)
        
        detailsView.clipsToBounds = true
        detailsView.layer.cornerRadius = 10
        
        transferOwnershipButton.layer.cornerRadius = 8
        transferOwnershipButton.layer.borderColor = UIColor.darkGray.cgColor
        transferOwnershipButton.layer.borderWidth = 1
        
        mapView.layer.cornerRadius = 8
        mapView.layer.borderColor = UIColor.darkGray.cgColor
        mapView.layer.borderWidth = 1
        
        updateUI()
        loadWatchesList()
        setupMapView()
    }
    
    private func updateUI(){
        watchDescription.text = selectedWatch.watchDescription
        watchImageView.image = UIImage(named: selectedWatch.imageURL)
        
        referenceText.text = selectedWatch.referenceNo
        caseMaterialText.text = selectedWatch.caseMaterial
        caseBackText.text = selectedWatch.caseBack
        dialColorText.text = selectedWatch.dialColor
        crystalText.text = selectedWatch.crystal
        resistanceText.text = selectedWatch.resistance
        movementTypeText.text = selectedWatch.movementType
        functionText.text = selectedWatch.functions
        
        
    }

    private func setupMapView(){
        var i = 0
        for item in dataArray{
            i = i + 1
            let annotation = MKPointAnnotation()
            annotation.coordinate = CLLocationCoordinate2D(latitude: item.locationLat, longitude: item.locationLong)
            mapView.addAnnotation(annotation)
            if dataArray.count == i{
                mapView.centerCoordinate = annotation.coordinate
            }
            
        }
    }
    
    private func loadWatchesList() {
        
        let watchHistory1 = WatchHistory(id: 1, referenceNo: "112312", locationLat: 46.96796, locationLong: 7.591101, startDate: "24.01.2019", endDate: "", ownerName: "Brain", city: "Bern")
        let watchHistory2 = WatchHistory(id: 1, referenceNo: "112312", locationLat: 47.497640, locationLong: 9.296758, startDate: "24.07.2018", endDate: "24.01.2019", ownerName: "Tim", city: "St Dallen")
        let watchHistory3 = WatchHistory(id: 1, referenceNo: "112312", locationLat: -33.936580, locationLong: 19.516775, startDate: "02.03.2016", endDate: "24.07.2018", ownerName: "Jacob", city: "Cape Town")
        let watchHistory4 = WatchHistory(id: 1, referenceNo: "112312", locationLat: 1.352424, locationLong: 103.838772, startDate: "24.01.2015", endDate: "02.03.2016", ownerName: "Benjamin", city: "Bishan")
//        let watchHistory5 = WatchHistory(id: 1, referenceNo: "112312", locationLat: 0.23789, locationLong: 102.464448, startDate: "24.05.2013", endDate: "24.01.2015", ownerName: "Kasun", city: "Colombo")
        
        self.dataArray = [watchHistory4, watchHistory3, watchHistory2, watchHistory1]
        tableHeight.constant = CGFloat(5 * HEIGHT_OF_ROW)
        self.title = ""
        
        self.reloadTable()
    }
    
    func reloadTable() {
        DispatchQueue.main.async {
            self.trackHistoryTableView.reloadData()
        }
    }
    @IBAction func tappedTransferOwnershipButoon(_ sender: Any) {
        
        let vc = TransferWatchViewController(nibName: "TransferWatchViewController", bundle: nil)
        vc.selectedWatch = selectedWatch
        vc.modalPresentationStyle = .overCurrentContext
        self.present(vc, animated: true) {
            
        }
    }
    @IBAction func tappedMapView(_ sender: Any) {
        let vc = MapHistoryViewController(nibName: "MapHistoryViewController", bundle: nil)
        vc.watchHistoryArray = dataArray
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
}


extension WatchDetailsViewController :  UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return dataArray.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        // Try to find reusable cell
        var cell = tableView.dequeueReusableCell(withIdentifier: "TrackHistoryTableViewCell") as? TrackHistoryTableViewCell
        
        // If not available instantiate new custom cell
        if (cell == nil) {
            var nibArray = NSArray()
            nibArray = Bundle.main.loadNibNamed("TrackHistoryTableViewCell", owner: self, options: nil)! as NSArray
            cell = nibArray.object(at: 0) as? TrackHistoryTableViewCell
        }
        
        let rowObj = self.dataArray[indexPath.row]
        // Setup Cells in table
        if indexPath.row == 0{
            cell?.trackIndicatorViewUpper.isHidden = true
            cell?.trackIcon.image = UIImage(named: "trackPresentOwnerIcon.png")
        }else{
            cell?.trackIndicatorViewUpper.isHidden = false
            cell?.trackIcon.image = UIImage(named: "trackPreviousOwnerIcon.png")
        }
        
        
        if indexPath.row == self.dataArray.count - 1{
            cell?.trackIndicatorViewLower.isHidden = true
        }else{
            cell?.trackIndicatorViewLower.isHidden = false
        }
        cell?.ownerName.text = rowObj.ownerName
        cell?.city.text = rowObj.city
        cell?.startDate.text = rowObj.startDate
        
        cell?.selectionStyle = .none
        return cell!
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return CGFloat(HEIGHT_OF_ROW)
    }
    
    //    MARK : UITableViewDelegate
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
}
