//
//  TrackHistoryTableViewCell.swift
//  Promize
//
//  Created by Isuru on 3/7/19.
//  Copyright © 2019 Creative Solutions. All rights reserved.
//

import UIKit

class TrackHistoryTableViewCell: UITableViewCell {

    @IBOutlet weak var startDate: UILabel!
    @IBOutlet weak var city: UILabel!
    @IBOutlet weak var ownerName: UILabel!
    
    @IBOutlet weak var trackIcon: UIImageView!
    @IBOutlet weak var trackIndicatorViewLower: UIView!
    @IBOutlet weak var trackIndicatorViewUpper: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
