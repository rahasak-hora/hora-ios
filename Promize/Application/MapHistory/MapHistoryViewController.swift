//
//  MapHistoryViewController.swift
//  Promize
//
//  Created by Isuru on 3/10/19.
//  Copyright © 2019 Creative Solutions. All rights reserved.
//

import UIKit
import MapKit

class MapHistoryViewController: BaseViewController, MKMapViewDelegate {

    var watchHistoryArray: [WatchHistory]!
    
    @IBOutlet weak var mapView: MKMapView!
    override func viewDidLoad() {
        super.viewDidLoad()

        mapView.delegate = self
        self.title = "Map history"
        setupMapView()
    }

    private func setupMapView(){
        var pointsArray = [CLLocationCoordinate2D]()
        for item in watchHistoryArray{
            let annotation = MKPointAnnotation()
            annotation.coordinate = CLLocationCoordinate2D(latitude: item.locationLat, longitude: item.locationLong)
            mapView.addAnnotation(annotation)
            mapView.centerCoordinate = annotation.coordinate
            
            pointsArray.append(annotation.coordinate)
        }
        let geodesicPolyline = MKGeodesicPolyline(coordinates: &pointsArray, count: watchHistoryArray.count)
        mapView.add(geodesicPolyline)
    }
    
    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        guard let polyline = overlay as? MKPolyline else {
            return MKOverlayRenderer()
        }
        
        let renderer = MKPolylineRenderer(polyline: polyline)
        renderer.lineWidth = 3.0
        renderer.alpha = 0.5
        renderer.strokeColor = UIColor.blue
        
        return renderer
    }
}
