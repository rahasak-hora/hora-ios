//
//  DocumentTableViewCell.swift
//  Promize
//
//  Created by Isuru on 2/8/19.
//  Copyright © 2019 Creative Solutions. All rights reserved.
//

import UIKit

class DocumentTableViewCell: UITableViewCell {

    @IBOutlet weak var modelName: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var referanceNo: UILabel!
    @IBOutlet weak var watchImageView: UIImageView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
//        self.modelName?.font = self.modelName?.font.fontWithName(name: Constants.MAIN_FONT_FAMILY.rawValue)
        self.descriptionLabel?.font = self.descriptionLabel?.font.fontWithName(name: Constants.MAIN_FONT_FAMILY.rawValue)
        self.referanceNo?.font = self.referanceNo?.font.fontWithName(name: Constants.MAIN_FONT_FAMILY.rawValue)
//        watchImageView.clipsToBounds = true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    

}
