//
//  WatchesListViewController.swift
//  Promize
//
//  Created by Isuru on 2/8/19.
//  Copyright © 2019 Creative Solutions. All rights reserved.
//

import UIKit

class WatchesListViewController: BaseViewController, UITableViewDelegate, UITableViewDataSource, UISearchResultsUpdating, UISearchBarDelegate {
    
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var searchPlaceHolder: UIView!
    
    var dataArray = [Watch]()
    var filteredArray = [Watch]()
    var shouldShowSearchResults = false
    var searchBar: CustomSearchBar!
    let HEIGHT_OF_ROW = 120
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Watches list" 
        self.setupUi()
        
        tblView.addSubview(self.refreshControl)
        
        self.loadWatchesList()
    }
    
    func setupUi() {
        self.configureCustomSearchController()
        
        let plusButton = UIButton(frame: CGRect(x: 0, y: 0, width: 25, height: 25))
        plusButton.setTitle("", for: .normal)
        plusButton.backgroundColor = UIColor.clear
        plusButton.layer.cornerRadius = 8.0
        plusButton.layer.masksToBounds = true
        plusButton.layer.borderWidth = 1
        plusButton.layer.borderColor = UIColor.white.cgColor
        plusButton.addTarget(self, action: #selector(tappedPlusbutton(button:)), for: .touchUpInside)
        
        var imageLogo = UIImage(named: "new_icon")
        imageLogo = imageLogo?.withRenderingMode(UIImageRenderingMode.alwaysOriginal)
        plusButton.setImage(imageLogo, for: .normal)
        
        let barButton = UIBarButtonItem(customView: plusButton)
        self.navigationItem.rightBarButtonItem = barButton

        
    }
    
    @objc func tappedPlusbutton(button: UIButton) {
        let view = AddWatchViewController(nibName: "AddWatchViewController", bundle: nil)
        self.present(view, animated: true) {
            
        }
    }
    
    func loadWatchesList() {
        
        let watch1 = Watch(id: 1, model: "ROLEX", watchDescription: "GMT Master II White Gold 40 mm Pepsi white Dial", referenceNo: "116719", caseMaterial: "18kt White Gold", caseBack: "Solid", dialColor: "Black Lacquer", crystal: "Scratch Resistant Sapphire", resistance: "100 meters/ 330 feet", movementType: "Automatic", functions: "Date, Hour, Minute, Second", imageURL: "watch1.png")
        let watch2 = Watch(id: 1, model: "SEIKO", watchDescription: "White Gold 40 mm Pepsi Blue Dial", referenceNo: "116720", caseMaterial: "White Gold", caseBack: "Solid", dialColor: "Lacquer", crystal: "Scratch Resistant Sapphire", resistance: "100 meters/ 330 feet", movementType: "Automatic", functions: "Date, Hour, Minute, Second", imageURL: "watch2.png")
        let watch3 = Watch(id: 1, model: "AUDEMARS PIGUET", watchDescription: "T-Sport V8 Chronorgaph Blue Dial Men's Watch", referenceNo: "116721", caseMaterial: "18kt White Gold", caseBack: "Solid", dialColor: "Black Lacquer", crystal: "Scratch Resistant Sapphire", resistance: "100 meters/ 330 feet", movementType: "Automatic", functions: "Date, Hour, Minute, Second", imageURL: "watch3.png")
        let watch4 = Watch(id: 1, model: "CREDOR", watchDescription: "GMT Master II White Gold 40 mm Pepsi Blue Dial", referenceNo: "116722", caseMaterial: "18kt White Gold", caseBack: "Solid", dialColor: "Black Lacquer", crystal: "Scratch Resistant Sapphire", resistance: "100 meters/ 330 feet", movementType: "Automatic", functions: "Date, Hour, Minute, Second", imageURL: "watch4.png")
//        let watch5 = Watch(id: 1, model: "GENEVE", watchDescription: "Seamaster 300 Automatic Black Dial", referenceNo: "116723", caseMaterial: "18kt White Gold", caseBack: "Solid", dialColor: "White Lacquer", crystal: "Scratch Resistant Sapphire", resistance: "100 meters/ 330 feet", movementType: "Automatic", functions: "Date, Hour, Minute, Second", imageURL: "watch5.png")
        let watch6 = Watch(id: 1, model: "GREUBEL FORSEY", watchDescription: "Oyster Perpetual Submariner Black Dial Black Cerachrom Bezel Steel Men's ", referenceNo: "116724", caseMaterial: "Red leather", caseBack: "Solid", dialColor: "Black Lacquer", crystal: "Scratch Resistant Sapphire", resistance: "100 meters/ 330 feet", movementType: "Automatic", functions: "Date, Hour, Minute, Second", imageURL: "watch6.png")
        let watch7 = Watch(id: 1, model: "GREUBEL FORSEY", watchDescription: "Seamaster 300 Automatic Black Dial Men's Watch 23330412101001", referenceNo: "116724", caseMaterial: "Metal", caseBack: "Solid", dialColor: "Black Lacquer", crystal: "Scratch Resistant Sapphire", resistance: "100 meters/ 330 feet", movementType: "Automatic", functions: "Date, Hour, Minute, Second", imageURL: "watch7.png")
//        let watch8 = Watch(id: 1, model: "Rolex", watchDescription: "GMT Master II White Gold 40 mm Pepsi Blue Dial", referenceNo: "116719", caseMaterial: "18kt White Gold", caseBack: "Solid", dialColor: "Black Lacquer", crystal: "Scratch Resistant Sapphire", resistance: "100 meters/ 330 feet", movementType: "Automatic", functions: "Date, Hour, Minute, Second", imageURL: "watch8.png")
        let watch9 = Watch(id: 1, model: "Rolex", watchDescription: "GMT Master II White Gold 40 mm Pepsi Blue Dial", referenceNo: "116719", caseMaterial: "Metal", caseBack: "Solid", dialColor: "Black Lacquer", crystal: "Scratch Resistant Sapphire", resistance: "100 meters/ 330 feet", movementType: "Automatic", functions: "Date, Hour, Minute, Second", imageURL: "watch9.png")
        let watch10 = Watch(id: 1, model: "HEUER", watchDescription: "White Gold 40 mm Pepsi Blue Dial", referenceNo: "116719", caseMaterial: "Black Metal", caseBack: "Solid", dialColor: "Black Lacquer", crystal: "Scratch Resistant Sapphire", resistance: "100 meters/ 330 feet", movementType: "Automatic", functions: "Date, Hour, Minute, Second", imageURL: "watch10.png")
        self.dataArray = [watch1, watch2, watch3, watch4, watch6, watch7, watch9, watch10]
        self.title = ""
    
        self.reloadTable()
    }
    
    func reloadTable() {
        DispatchQueue.main.async {
            self.tblView.reloadData()
            self.refreshControl.endRefreshing()
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if shouldShowSearchResults {
            return filteredArray.count
        }
        else {
            return dataArray.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        // Try to find reusable cell
        var cell = tableView.dequeueReusableCell(withIdentifier: "DocumentTableViewCell") as? DocumentTableViewCell
        
        // If not available instantiate new custom cell
        if (cell == nil) {
            var nibArray = NSArray()
            nibArray = Bundle.main.loadNibNamed("DocumentTableViewCell", owner: self, options: nil)! as NSArray
            cell = nibArray.object(at: 0) as? DocumentTableViewCell
        }
        
        // Select Array to use to load table
        let tableArray : [Watch] = shouldShowSearchResults ? self.filteredArray : self.dataArray
        let rowObj = tableArray[indexPath.row]
        // Setup Cells in table
        cell?.modelName.text = rowObj.model
        cell?.watchImageView.image = UIImage(named: rowObj.imageURL)
        cell?.descriptionLabel.text = rowObj.watchDescription
        cell?.referanceNo.text = rowObj.referenceNo
        
        cell?.selectionStyle = .none
        return cell!
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return CGFloat(HEIGHT_OF_ROW)
    }
    
    //    MARK : UITableViewDelegate
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        searchBarCancelButtonClicked(searchBar)
        var selectedObj:Watch?
        if filteredArray.count > 0 {
            selectedObj = filteredArray[indexPath.row]
        }
        else {
            selectedObj = dataArray[indexPath.row]
        }
        
        let viewC = WatchDetailsViewController(nibName: "WatchDetailsViewController", bundle: nil)
        viewC.selectedWatch = selectedObj
        self.navigationController?.pushViewController(viewC, animated: true)
    }
    
    func configureCustomSearchController() {
        self.searchBar = CustomSearchBar(searchBarFrame: CGRect(x: 0.0,y: 0.0, width: tblView.frame.size.width, height: 56.0), searchBarFont: UIFont(name: Constants.MAIN_FONT_FAMILY.rawValue, size: 22.0)!, searchBarTextColor: UIColor.fromHex(HexColors.PRIMARY_COLOR.rawValue), searchBarTintColor: UIColor.fromHex(HexColors.WHITE_COLOR.rawValue), placeholderText: "Search Watches")
        self.searchBar.delegate = self
        self.searchPlaceHolder.addSubview(searchBar)
    }
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        shouldShowSearchResults = true
        filteredArray.removeAll()
        filteredArray = dataArray
        self.reloadTable()
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        shouldShowSearchResults = false
        self.searchBar.text = ""
        searchBar.resignFirstResponder()
        self.reloadTable()
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        if !shouldShowSearchResults {
            shouldShowSearchResults = true
            self.reloadTable()
        }
    }
    
    func updateSearchResults(for searchController: UISearchController) {
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchText.isEmpty{
            filteredArray.removeAll()
            filteredArray = dataArray
        } else{
            filteredArray = dataArray.filter({ (watchRecord) -> Bool in
                return watchRecord.model.lowercased().range(of:searchText.lowercased()) != nil
            })
        }
        
        self.reloadTable()
    }
    
    //    MARK : Pull to refresh
    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self,
                                 action: #selector(self.handleRefresh(_:)),
                                 for: UIControlEvents.valueChanged)
        refreshControl.tintColor = UIColor.blue
        
        return refreshControl
    }()
    
    @objc func handleRefresh(_ refreshControl: UIRefreshControl) {
        
        DispatchQueue.global(qos: .background).async {
            
            self.loadWatchesList()
            
        }
        
    }
}

