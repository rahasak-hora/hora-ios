//
//  BaseViewController.swift
//  Promize
//
//  Created by Lakmal Caldera on 5/8/18.
//  Copyright © 2018 Creative Solutions. All rights reserved.
//

import UIKit

class BaseViewController : ApplicationViewController {

    // Defaults
    var isNavBarHidden = false

    override func viewDidLoad() {
        super.viewDidLoad()
        self.setEmptyBackButton()

        // Ensure all view controllers frame stays within bounds
        edgesForExtendedLayout = []
        
        let statusBarColor = UIColor.fromHex(HexColors.PRIMARY_COLOR.rawValue)
        self.view.backgroundColor = statusBarColor
        self.navigationController?.view.backgroundColor = statusBarColor
    }

    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(self.isNavBarHidden, animated: false)
        super.viewWillAppear(animated)
        
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationItem.title = self.title
    }

    func setNavBarHidden(_ hide: Bool) {
        self.isNavBarHidden = hide
    }

    func setEmptyBackButton() {
        self.navigationItem.backBarButtonItem =
            UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
    }
 
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
}
