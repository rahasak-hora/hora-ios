//
//  Promize.swift
//  Promize
//
//  Created by eranga on 11/16/18.
//  Copyright © 2018 Creative Solutions. All rights reserved.
//

import Foundation

class Promize {
    let id: Int64
    var zid: String = ""
    var user: String = ""
    var account: String = ""
    var amount: String = ""
    var salt: String = ""
    var direction: String = "SENT"
    var timestamp: Int64
    
    init(id: Int64) {
        self.id = id
        self.timestamp = TimeUtil.sharedInstance.timestamp()
    }
}
