//
//  Account.swift
//  Promize
//
//  Created by eranga on 11/16/18.
//  Copyright © 2018 Creative Solutions. All rights reserved.
//

import Foundation

class Account {
    let id: Int64
    var user: String = ""
    var bank: String = ""
    var bankCode: String = ""
    var no: String = ""
    
    init(id: Int64) {
        self.id = id
    }
}
