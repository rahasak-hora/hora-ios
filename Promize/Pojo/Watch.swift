//
//  Document.swift
//  Promize
//
//  Created by Isuru on 2/8/19.
//  Copyright © 2019 Creative Solutions. All rights reserved.
//

import UIKit

class Watch: NSObject {
    var id: Int
    var model: String
    var watchDescription: String
    var referenceNo: String
    var caseMaterial: String
    var caseBack: String
    var dialColor: String
    var crystal: String
    var resistance: String
    var movementType: String
    var functions: String
    var imageURL: String
//    var watchHistory: [WatchHistory]
    
    init(id: Int, model: String, watchDescription: String, referenceNo: String, caseMaterial: String, caseBack: String, dialColor: String, crystal: String, resistance: String, movementType: String, functions: String, imageURL: String) {
        self.id = id
        self.model = model
        self.watchDescription = watchDescription
        self.referenceNo = referenceNo
        self.caseMaterial = caseMaterial
        self.caseBack = caseBack
        self.dialColor = dialColor
        self.crystal = crystal
        self.resistance = resistance
        self.movementType = movementType
        self.functions = functions
        self.imageURL = imageURL
    }
}

class WatchHistory: NSObject {

    var id: Int
    var referenceNo: String
    var locationLat: Double
    var locationLong: Double
    var startDate: String
    var endDate: String
    var ownerName: String
    var city: String
    
    init(id: Int, referenceNo: String, locationLat: Double, locationLong: Double, startDate: String, endDate: String, ownerName: String, city: String){
        self.id = id
        self.referenceNo = referenceNo
        self.locationLat = locationLat
        self.locationLong = locationLong
        self.startDate = startDate
        self.endDate = endDate
        self.ownerName = ownerName
        self.city = city
    }
}
