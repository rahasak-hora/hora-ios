//
//  Colors.swift
//  Promize
//
//  Created by Lakmal Caldera on 5/10/18.
//  Copyright © 2018 Creative Solutions. All rights reserved.
//

import Foundation

enum HexColors: UInt32 {
    case PRIMARY_COLOR = 0x1e2126 //5287ED //0x566898//f5944e
    case WHITE_COLOR = 0xFFFFFF
    
//    case DOC_PENDING_COLOR = 0x00B400
//    case DOC_INPROGRESS_COLOR = 0x1385C2
//    case DOC_REJECTED_COLOR = 0xCC2900
//    case DOC_COMPLETED_COLOR = 0xFFFFFF
}
