//
//  Strings.swift
//  Promize
//
//  Created by Lakmal Caldera on 5/10/18.
//  Copyright © 2018 Creative Solutions. All rights reserved.
//

import Foundation

enum Constants: String {
    case MAIN_FONT_FAMILY = "GeosansLight"
    case IMAGES_DIR = "PromizeImages"
}

enum DocFilterType: String {
    case all = "All"
    case pending = "Pending"
    case inProgress = "In Progress"
    case rejected = "Rejected"
    case compeleted = "Completed"
}
